<?php

namespace App\Http\Controllers;

use App\Services\ZohoCRMService;
use Illuminate\Http\Request;

class ZohoCRMController extends Controller
{
    private $zohoCRMService;

    public function __construct(ZohoCRMService $zohoCRMService)
    {
        $this->zohoCRMService = $zohoCRMService;
    }

    public function createDeal(Request $request)
    {
        $dealName = $request->input('dealName');
        $dealStage = $request->input('dealStage');
        $accountName = $request->input('accountName');

        $dealResponse = $this->zohoCRMService->createRequest('Deals', [
            ['Deal_Name' => $dealName, 'Stage' => $dealStage, 'Account_Name' => $accountName]
        ]);

        if ($dealResponse['success']) {
            return response()->json(['message' => 'Deal created successfully']);
        } else {
            return response()->json(['error' => 'Error creating deal']);
        }
    }

    public function createAccount(Request $request)
    {
        $accountName = $request->input('accountName');
        $accountWebsite = $request->input('accountWebsite');
        $accountPhone = $request->input('accountPhone');

        $accountResponse = $this->zohoCRMService->createRequest('Accounts', [
            [
                'Account_Name' => $accountName,
                'Website' => $accountWebsite,
                'Phone' => $accountPhone
            ]
        ]);

        if ($accountResponse['success']) {
            return response()->json(['message' => 'Account created successfully']);
        } else {
            return response()->json(['error' => 'Error creating account']);
        }
    }

    public function getAccounts()
    {
        $accountResponse = $this->zohoCRMService->createRequest('Accounts/bulk', [
            ['page' => 1, 'per_page' => 11]
        ]);

        if ($accountResponse['success']) {
            return response()->json($accountResponse);
        } else {
            return response()->json(['error' => 'Error getting accounts']);
        }
    }
}
