<?php

namespace App\Services;

use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;

class ZohoCRMService
{
    const REFRESH_TOKEN = 'refresh_token';
    private $apiUrl;
    private $tokenUrl;
    private $client;
    private $user;

    public function __construct()
    {
        $this->apiUrl = Config::get('app.zoho_crm.api_url');
        $this->tokenUrl = Config::get('app.zoho_crm.token_url');
        $this->client = new Client;
        $this->user = User::whereZohoClientId(env('ZOHO_CLIENT_ID'))->first();
    }

    public function createRequest($moduleName, $data)
    {
        $accessToken = $this->user->zoho_access_token;

        if (!$accessToken) {
            return [
                'success' => false,
                'response' => 'Access token not found'
            ];
        }

        try {
            $response = $this->client->post($this->apiUrl . $moduleName, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $accessToken,
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'data' => $data
                ]
            ]);

            return $this->handleResponse($response, $moduleName, $data);
        } catch (ClientException $e) {
            return $this->handleResponse($e->getResponse(), $moduleName, $data);
        }
    }

    private function handleResponse($response, $moduleName, $data)
    {
        $statusCode = $response->getStatusCode();
        $responseData = json_decode($response->getBody()->getContents(), true);

        if ($statusCode == 401 && $responseData['code'] == 'INVALID_TOKEN') {
            $refreshToken = $this->refreshToken($this->user);
            if (!$refreshToken) {
                return [
                    'success' => false,
                    'response' => 'Error updating token'
                ];
            }
            return $this->createRequest($moduleName, $data);
        }

        return [
            'success' => $statusCode,
            'response' => $responseData
        ];
    }

    private function refreshToken(User $user)
    {
        $response = $this->client->post($this->tokenUrl, [
            'form_params' => [
                'grant_type' => self::REFRESH_TOKEN,
                'client_id' => $user->zoho_client_id,
                'client_secret' => $user->zoho_client_secret,
                'refresh_token' => $user->zoho_refresh_token,
                'redirect_uri' => $user->zoho_redirect_uri,
            ],
        ]);

        $responseData = json_decode($response->getBody(), true);

        if (isset($responseData['access_token'])) {
            $accessToken = $responseData['access_token'];
            $user->zoho_access_token = $accessToken;
            $user->save();
            return $accessToken;
        } else {
            return null;
        }
    }
}
