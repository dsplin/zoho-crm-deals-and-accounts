<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Zoho User',
            'email' => 'zoho@example.com',
            'password' => Hash::make('password'),
            'zoho_access_token' => env('ZOHO_ACCESS_TOKEN'),
            'zoho_refresh_token' => env('ZOHO_REFRESH_TOKEN'),
            'zoho_client_id' => env('ZOHO_CLIENT_ID'),
            'zoho_client_secret' => env('ZOHO_CLIENT_SECRET'),
            'zoho_redirect_uri' => env('ZOHO_REDIRECT_URI'),
        ]);
    }
}
