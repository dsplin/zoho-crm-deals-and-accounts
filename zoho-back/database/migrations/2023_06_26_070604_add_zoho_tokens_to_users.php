<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('zoho_access_token')->nullable();
            $table->string('zoho_refresh_token')->nullable();
            $table->string('zoho_client_id');
            $table->string('zoho_client_secret');
            $table->string('zoho_redirect_uri');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('zoho_access_token');
            $table->dropColumn('zoho_refresh_token');
        });
    }
};
