<?php

use App\Http\Controllers\ZohoCRMController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/account', [ZohoCRMController::class, 'createAccount']);
Route::post('/accounts', [ZohoCRMController::class, 'getAccounts']);
Route::post('/deal', [ZohoCRMController::class, 'createDeal']);
