# Creating a Web Form for Zoho CRM Deals and Accounts using Vue.js and Laravel

## Requirements

Before starting the deployment, make sure your system meets the following requirements:

- Node.js (14)
- Docker
- Docker Compose

## Deployment

To deploy the project, follow these steps:

1. Clone the repository:
   ```shell
   git clone git@gitlab.com:dsplin/zoho-crm-deals-and-accounts.git
   
2. Navigate to the project directory:
   ```shell
   cd zoho-crm-deals-and-accounts
   
3. Start the Docker containers:
   ```shell
   docker-compose up -d

4. Go to the zoho-back (laravel) container:
   ```shell
   docker-compose exec zoho-back bash
   
5. Run these commands
   ```shell
   composer install
   php artisan migrate
   php artisan db:seed

6. Once the containers are successfully running, you can access your application using the following URL:
   ```shell
   http://localhost:8080

